using MyVod.Common.BuildingBlocks.Ddd;

namespace MyVod.Domain.Library;

// possible projection -> public class Invoice : Entity<Guid>, AggregateRoot<Guid>
// {
//     
// }

public class InvoiceHeader : Entity<Guid>, AggregateRoot<Guid>
{
    public InvoiceParty From { get; private set; }
    public InvoiceParty To { get; private set; }
}

public class InvoiceItems : Entity<Guid>, AggregateRoot<Guid>
{
    public List<InvoiceListItem> Items = new List<InvoiceListItem>();
    public bool IsExternal { get; set; }
}

public class InvoiceListItem : Entity<Guid>
{
    public string Name { get; set; }
    public int Quantity { get; set; }
    public int Price { get; set; }
    public int Tax { get; set; }
}

public class InvoiceParty : ValueObject<InvoiceParty>
{
    protected override IEnumerable<object> GetEqualityComponents()
    {
        throw new NotImplementedException();
    }
}

public class InvoiceValue : Entity<Guid>, AggregateRoot<Guid>
{
}