using MediatR;
using MyVod.Common.BuildingBlocks.Ddd;
using MyVod.Domain.SharedKernel;

namespace MyVod.Domain.Legal.Domain;

public sealed class License : Entity<Guid>, AggregateRoot<Guid>
{
    public DateRange Range { get; private set; } = null!;
    public MovieId MovieId { get; private set; }

    public License(DateRange range, MovieId movieId)
    {
        Range = range;
        MovieId = movieId;

        Id = Guid.NewGuid();

        Raise(new LicenseCreated(Id, MovieId, Range));
    }
}