using MediatR;
using MyVod.Domain.SharedKernel;

namespace MyVod.Domain.Library.Application.Listeners;

public class LicenseCreatedListener : INotificationHandler<LicenseCreated>
{
    private readonly IMovieService _movieService;

    public LicenseCreatedListener(IMovieService movieService)
    {
        _movieService = movieService;
    }
    
    public async Task Handle(LicenseCreated notification, CancellationToken cancellationToken)
    {
        await _movieService.Publish(notification.MovieId, notification.LicenseId, notification.Range);
    }
}