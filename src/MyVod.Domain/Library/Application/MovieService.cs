using MyVod.Common.BuildingBlocks.Ddd;
using MyVod.Domain.Library.Domain;
using MyVod.Domain.Library.Infrastructure;
using MyVod.Domain.SharedKernel;

namespace MyVod.Domain.Library.Application;

public interface IMovieService : IApplicationService
{
    Task<Movie?> Get(MovieId id);
    Task Publish(MovieId id, Guid licenseId, DateRange range);
}

public class MovieService : IMovieService
{
    private readonly IMovieRepository _repository;

    public MovieService(IMovieRepository repository)
    {
        _repository = repository;
    }
    
    public Task<Movie?> Get(MovieId id)
    {
        return _repository.Get(id);
    }

    public Task Publish(MovieId id, Guid licenseId, DateRange range)
    {
        return Task.CompletedTask;
    }
}