using EnsureThat;
using MediatR;
using MyVod.Common.BuildingBlocks.Ddd;
using MyVod.Domain.SharedKernel;

namespace MyVod.Domain.Library.Domain;

public sealed class Movie : Entity<MovieId>, AggregateRoot<MovieId>
{
    public Title Title { get; private set; }

    public Director? Director { get; private set; }

    [Obsolete("Only for EF Core", true)]
    private Movie()
    { }
    
    public Movie(Title title)
    {
        Ensure.That(title).IsNotNull();
        
        Title = title;
        Id = MovieId.New();
        
        Raise(new Events.MovieCreated(Id));
    }

    public static class Events
    {
        public class MovieCreated : INotification
        {
            public Guid Id { get; private set; } = Guid.NewGuid();
            public MovieId MovieId { get; private set; }
            public DateTime CreatedAt { get; private set; } = DateTime.UtcNow;

            public MovieCreated(MovieId movieId)
            {
                MovieId = movieId;
            }
        }
    }
}