using MyVod.Common.BuildingBlocks.Ddd;
using MyVod.Domain.Library.Domain;
using MyVod.Domain.SharedKernel;

namespace MyVod.Domain.Library.Infrastructure;

public interface IMovieRepository : IRepository
{
    Task<Movie?> Get(MovieId id);
}
