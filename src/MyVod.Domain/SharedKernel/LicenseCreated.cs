using MediatR;

namespace MyVod.Domain.SharedKernel;

public class LicenseCreated : INotification
{
    public Guid Id { get; private set; } = Guid.NewGuid();
    public Guid LicenseId { get; private set; }
    public MovieId MovieId { get; private set; }
    public DateRange Range { get; private set; }

    public LicenseCreated(Guid licenseId, MovieId movieId, DateRange range)
    {
        LicenseId = licenseId;
        MovieId = movieId;
        Range = range;
    }
}