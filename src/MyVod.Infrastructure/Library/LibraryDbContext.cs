using MediatR;
using Microsoft.EntityFrameworkCore;
using MyVod.Common.BuildingBlocks.Extensions;
using MyVod.Domain.Library.Domain;

namespace MyVod.Infrastructure.Library;

public class LibraryDbContext : DbContext, Common.BuildingBlocks.EfCore.IUnitOfWork
{
    private readonly IMediator _mediator;
    public DbSet<Movie> Movies { get; set; } = null!;

    public LibraryDbContext(IMediator mediator, DbContextOptions<LibraryDbContext> options) : base(options)
    {
        _mediator = mediator;
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.ApplyConfiguration(new MovieEntityTypeConfiguration());
        modelBuilder.ApplyConfiguration(new DirectorEntityTypeConfiguration());
    }

    public async Task<bool> SaveEntitiesAsync(CancellationToken cancellationToken = default)
    {
        await _mediator.DispatchDomainEventsAsync<LibraryDbContext>(this);
        
        await SaveChangesAsync(cancellationToken);

        return true;
    }
}