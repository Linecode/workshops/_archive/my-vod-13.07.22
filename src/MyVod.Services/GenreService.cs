using Microsoft.EntityFrameworkCore;
using MyVod.Infrastructure;
using MyVod.Infrastructure.Models;

namespace MyVod.Services;

public interface IGenreService
{
    Task Create(Genre genre);
    Task Delete(Genre genre);
    Task Delete(Guid id);
    Task Update(Genre genre);
    Task<Genre?> Get(Guid id);
    IQueryable<Genre> Get();
}

public class GenreService : IGenreService
{
    private readonly MoviesDbContext _context;

    public GenreService(MoviesDbContext context)
    {
        _context = context;
    }
    
    public async Task Create(Genre genre)
    {
        genre.Id = Guid.NewGuid();

        _context.Add(genre);
        await _context.SaveChangesAsync();
    }

    public async Task Delete(Genre genre)
    {
        _context.Genres.Remove(genre);

        await _context.SaveChangesAsync();
    }

    public async Task Delete(Guid id)
    {
        _context.Genres.Remove(new Genre { Id = id });

        await _context.SaveChangesAsync();
    }

    public async Task Update(Genre genre)
    {
        _context.Genres.Update(genre);
        await _context.SaveChangesAsync();
    }

    public async Task<Genre?> Get(Guid id)
    {
        var genre = await _context.Genres.FindAsync(id);

        return genre;
    }

    public IQueryable<Genre> Get()
    {
        return _context.Genres.AsNoTracking().AsQueryable();
    }
}