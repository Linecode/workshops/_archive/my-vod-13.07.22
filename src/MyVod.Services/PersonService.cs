using Microsoft.EntityFrameworkCore;
using MyVod.Infrastructure;
using MyVod.Infrastructure.Models;

namespace MyVod.Services;

public interface IPersonService
{
    Task Create(Person genre);
    Task Delete(Person genre);
    Task Delete(Guid id);
    Task Update(Person genre);
    Task<Person?> Get(Guid id);
    IQueryable<Person> Get();
}
public class PersonService : IPersonService
{
    private readonly MoviesDbContext _context;

    public PersonService(MoviesDbContext context)
    {
        _context = context;
    }
    
    public async Task Create(Person person)
    {
        person.Id = Guid.NewGuid();

        _context.Add(person);
        await _context.SaveChangesAsync();
    }

    public async Task Delete(Person person)
    {
        _context.Persons.Remove(person);

        await _context.SaveChangesAsync();
    }

    public async Task Delete(Guid id)
    {
        _context.Persons.Remove(new Person { Id = id });

        await _context.SaveChangesAsync();
    }

    public async Task Update(Person person)
    {
        _context.Persons.Update(person);
        await _context.SaveChangesAsync();
    }

    public async Task<Person?> Get(Guid id)
    {
        var person = await _context.Persons.FindAsync(id);

        return person;
    }

    public IQueryable<Person> Get()
    {
        return _context.Persons.AsNoTracking().AsQueryable();
    }
}