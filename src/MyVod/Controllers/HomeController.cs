﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using MyVod.Domain;
using MyVod.Domain.Library.Domain;
using MyVod.Domain.SharedKernel;
using MyVod.Models;
using MyVod.Services;

namespace MyVod.Controllers;

public class HomeController : Controller
{
    private readonly ILogger<HomeController> _logger;
    private readonly IMovieService _movieService;
    private readonly Domain.Library.Application.IMovieService _newMovieService;

    public HomeController(ILogger<HomeController> logger,
        IMovieService movieService,
        Domain.Library.Application.IMovieService newMovieService)
    {
        _logger = logger;
        _movieService = movieService;
        _newMovieService = newMovieService;
    }

    public async Task<IActionResult> Index()
    {
        var movies = await _movieService.GetLive();
        return View(movies);
    }

    public IActionResult Privacy()
    {
        return View();
    }

    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error()
    {
        return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
    }

    [HttpGet("/test/{id:guid}")]
    public async Task<IActionResult> Test(MovieId id)
    {
        var movie = await _newMovieService.Get(id);

        return Json(movie);
    }
}
