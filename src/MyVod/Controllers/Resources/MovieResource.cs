using MyVod.Domain.Library.Domain;

namespace MyVod.Controllers.Resources;

public class MovieResource
{
    public DateOnly date = DateOnly.FromDateTime(DateTime.Now);
    
    public static MovieResource From(Movie movie) => new();
}

// Do obiektu Movie trzeba dodac rzeczy pobierane z bazy danych takie jak:  
// - Desceription
// - Cover
// - Trailer
// - Dostępnośc
//
//
// * - Dostepnosc wyrazic jako obiekty DateOnly bez zmiany bazy danych
// 